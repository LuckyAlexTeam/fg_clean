<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'            => $this->primaryKey(),
            'company_name'  => $this->string(255),
            'first_name'    => $this->string(32)->notNull(),
            'last_name'     => $this->string(32)->notNull(),
            'middle_name'   => $this->string(32),
            'desc'          => $this->text(),
            'phone'         => $this->string(11)->notNull(),
            'phone2'        => $this->string(11),
            'lang'          => $this->string(2)->notNull(),
            'email'         => $this->string(64)->notNull()->unique(),
            'city_id'       => $this->integer(11),
            'status'        => $this->integer(1),
            'image_main'    => $this->string(20)->unique(),
            'images'        => $this->string(20)->unique(),
            'password_hash' => $this->string()->notNull(),

            'auth_key' => $this->string(32)->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email_confirm_token' => $this->string()->unique(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

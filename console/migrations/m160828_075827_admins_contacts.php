<?php

use yii\db\Migration;

class m160828_075827_admins_contacts extends Migration
{
    public function up()
    {
        $tableOptions = NULL;

        $this->createTable('admin_contacts', [
            'category_question' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'en' => $this->string()->notNull(),
            'ru' => $this->string()->notNull(),
            'fr' => $this->string()->notNull(),
            'ch' => $this->string()->notNull()
        ], $tableOptions);
        $this->batchInsert('admin_contacts', ['email', 'en', 'ru'], [
            ['info@fashiongreatness.com', 'General question', 'Общие вопросы'],
            ['support@fashiongreatness.com', 'Technical support', 'Техническая поддержка'],
            ['contacts@fashiongreatness.com', 'Partnership/Cooperation', 'Партенрство / сотрудничество'],
            ['contacts@fashiongreatness.com', 'Events', 'Мероприятия'],
            ['support@fashiongreatness.com', 'Claims and proposal', 'Претензии и предложения'],
            ['contacts@fashiongreatness.com', 'Press Services', 'Пресс-службы'],
            ['info@fashiongreatness.com', 'Other', 'Другие'],
            ]);
    }

    public function down()
    {
        $this->dropTable('admin_contacts');

    }

}

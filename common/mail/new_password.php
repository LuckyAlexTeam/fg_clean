<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2>Здравствуйте, <?= $name?>.</h2>
<p>Ваш пароль изменен на: <b><?= $password?></b></p>
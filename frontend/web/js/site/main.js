
var tek_slid = 0;
var mou_dow = 0;
var mou_x = 0;
var mou_t = 0;
var napr = '';
$(document).ready(function() {

    var lastId,
    topMenu = $("#top-menu"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 300);
        e.preventDefault();
    });
    // Bind to scroll
    $(window).scroll(function(){
        // Get container scroll position
        var fromTop = $(this).scrollTop()+topMenuHeight;

        // Get id of current scroll item
        var cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
            return this;
        });
        // Get the id of the current element
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass("active")
                .end().filter("[href='#"+id+"']").parent().addClass("active");
        }
    });


    $('#b6_but').click( function() {
        p1 = $('#b6_1').val();
        p2 = $('#b6_2').val();
        p3 = $('#b6_3').val();
        p4 = $('#b6_4').val();
        //alert(p1+p2+p3+p4);
        $('#b6_soo').css('display','block');
        $.ajax({
            url: 'index.php?r=site/soo',
            type: 'post',
            async: true,
            data: 'nam='+$('#b6_1').val() +'&mai='+$('#b6_2').val() +'&sel='+$('#b6_3').val() +'&soo='+$('#b6_4').val(),
            dataType: 'json',
            success: function(json) {

            }
        });
        /*
        sme1 = $('.item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""));
        sme3 = sme2 * $(this).attr('sme');
        tek_slid = $(this).attr('sme');
        $("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
        $(".c-p").removeClass("current");
        $(this).parent().addClass("current");
        */
        return false;
    });
        $('.slid').click( function() {
        sme1 = $('.item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""));
        sme3 = sme2 * $(this).attr('sme');
        tek_slid = $(this).attr('sme');
        $("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
        $(".c-p").removeClass("current");
        $(this).parent().addClass("current");
        return false;
    });
    $(window).resize(function () {
        sme1 = $('.item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""));
        sme3 = sme2 * tek_slid;
        $("#ulsl").css('left',sme3);
        //console.log(tek_slid, sme1);
    });
        //
    $('.item').mousemove(function(eventObject){
        if (mou_dow == 1) {
            sme1 = $("#ulsl").css('left');
            sme2 = Number(sme1.replace(/\D+/g,"")); // Math.abs(
            mou_t = eventObject.pageX;
            //if (mou_x > mou_t) { smed = mou_x - mou_t; } else { smed = mou_t - mou_x;};
            smed = mou_x - mou_t;
            sme3 = (sme2 + Number(smed)) * (-1);
            $("#ulsl").css('left',sme3);
            mou_x = mou_t;
            if (smed > 0) {napr = 'L'};
            if (smed < 0) {napr = 'R'};
            if (smed == 0) {napr = ''};
            //console.log('mou_dow=' +mou_dow+ ' mou_x=' + mou_x + ' sme1=' + sme1 + " sme2="+sme2 + " smed="+smed + 'napr='+ napr + ' X='+eventObject.pageX            + " Y="+eventObject.pageY);
        };
    });
        $('.item').mousedown(function(eventObject){
        mou_dow = 1; mou_x = eventObject.pageX; mou_t = mou_x;
    });
        $('.item').mouseup(function(eventObject){
        mou_dow = 0;
        if ((napr == 'L') && (tek_slid != -4)) { --tek_slid; };
        if ((napr == 'R') && (tek_slid != 0) ) { ++tek_slid; };
        $('#s'+tek_slid).click();
    });
        $('.item').mouseout(function(){
        mou_dow = 0; napr = ''; // console.log(mou_dow);
    });
        $('#but_join').click( function() {
        $('#lay-hid_join').css('display','block');
        $("#lay-hid_join").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
        $("#lay-hid_join_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
    });
        $('#lay-hid_join_close').click( function() {
        $("#lay-hid_join").fadeOut('slow', function() {
            $("#lay-hid_join_forma").css('margin-top','-20%');
            $('#lay-hid_join').css('display','none');
            $('#lay-hid_join').css('opacity','0.1');
        });
    });
        $('#reg_mai').change( function() {
        //alert('ok');
        $.ajax({
            url: 'index.php?r=site/email',
            type: 'post',
            async: true,
            data: 'email='+$('#reg_mai').val(),
            dataType: 'json',
            success: function(json) {
                if (json['otv'] == "ok") { $('#error').text(""); return;};
                $('#error').text("Такой email уже есть");
            }
        });
    });
        $('[id ^= "reg_pa"]').change( function() {
        if ($('#reg_pa1').prop('checked')) {
            $('#reg_gr1').css('display','none');
        } else {
            $('#reg_gr1').css('display','inline-block');
        };
    });

        $('#but_signin').click( function() {
        $('#lay-hid_signin').css('display','block');
        $("#lay-hid_signin").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
        $("#lay-hid_signin_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
    });
        $('#lay-hid_signin_close').click( function() {
        $("#lay-hid_signin").fadeOut('slow', function() {
            $("#lay-hid_signin_forma").css('margin-top','-20%');
            $('#lay-hid_signin').css('display','none');
            $('#lay-hid_signin').css('opacity','0.1');
        });
    });
        $('#form_sig').submit( function() {
        soo = "";
        if ($('#sig_mai').val() == "") {soo += "<br>Email пустой"; };
        if ($('#sig_pas').val() == "") {soo += "<br>Пароль пустой"; };
        if (soo != "") {$('#sig_error').html(soo); return false;};
        return true;
    });
        $('#lay-hid_sigin_fyp').click( function() {
        $('#lay-hid_signin_close').click();         $('#lay-hid_forgot').css('display','block');
        $("#lay-hid_forgot").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
        $("#lay-hid_forgot_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
    });
        $('#lay-hid_forgot_close').click( function() {
        $("#lay-hid_forgot").fadeOut('slow', function() {
            $("#lay-hid_forgot_forma").css('margin-top','-20%');
            $('#lay-hid_forgot').css('display','none');
            $('#lay-hid_forgot').css('opacity','0.1');
        });
    });
    $('#join1').click( function() {
        $('#reg_pa1').prop('checked',false);
        $('#reg_pa2').prop('checked',true);
        $('#reg_pa3').prop('checked',false);
        $('#reg_pa4').prop('checked',false);
        $('#reg_gr1').css('display','inline-block');
        $('#but_join').click();
    });
    $('#join2').click( function() {
        $('#reg_pa1').prop('checked',false);
        $('#reg_pa2').prop('checked',false);
        $('#reg_pa3').prop('checked',true);
        $('#reg_pa4').prop('checked',false);
        $('#reg_gr1').css('display','inline-block');
        $('#but_join').click();
    });
    $('#join3').click( function() {
        $('#reg_pa1').prop('checked',false);
        $('#reg_pa2').prop('checked',false);
        $('#reg_pa3').prop('checked',false);
        $('#reg_gr1').css('display','inline-block');
        $('#reg_pa4').prop('checked',true);
        $('#but_join').click();
    });
    kol_team = 4;
    left_team = 0;
    $('#but_team_prev').click( function() {
        wid1 = $('#team_slid').css('width');
        wid2 = Number(wid1.replace(/\D+/g,""));
        sme1 = $('.team-item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""))+20;
        //www1 = sme2 * kol_team;
        kkk1 = Math.floor(wid2 / sme2);
        kkk2 = wid2 / sme2;
        mmm1 = kol_team - kkk1;
        //console.log('prev =>>= wid2='+wid2 +' , sme2='+sme2  +' , kkk1='+mmm1 +' , mmm1='+mmm1 +' , left_team='+left_team);
        aaa = '=>>= окно wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
        //$('#otl').text(aaa);
        if (left_team < (mmm1-0) ) {
                                                    //if (left_team < (kol_team -3)) {
            ++left_team;
            //sme3 = sme2 * $(this).attr('sme');
            sme3 = left_team * sme2;
            $('#team_' + left_team).css('display', 'none');
            //$('#team_' + left_team).css('margin-left', '-'+sme3+'px');
        };
    });
    $('#but_team_next').click( function() {
        wid1 = $('#team_slid').css('width');
        wid2 = Number(wid1.replace(/\D+/g,""));
        sme1 = $('.team-item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""))+20;
        www1 = sme2 * kol_team;
        kkk1 = Math.floor(wid2 / sme2);
        kkk2 = wid2 / sme2;
        mmm1 = kol_team - kkk1;
        //console.log('prev =>>= wid2='+wid2 +'sme2='+sme2 +'www1='+www1 +'kkk1='+mmm1 +'  mmm1='+mmm1 +'left_team='+left_team);
        aaa = '=>>= окно wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
        //$('#otl').text(aaa);
        if (left_team > 0) {
            $('#team_' + left_team).css('display', 'inline-block');
            --left_team;
            sme3 = left_team * sme2;
            //$('#team_1').css('margin-left', '-'+sme3+'px');
            //$('#team_' + left_team).css('display', 'inline-block');
        };
    });
    $(window).resize(function () {
        wid1 = $('#team_slid').css('width');
        wid2 = Number(wid1.replace(/\D+/g,""));
        sme1 = $('.team-item').css('width');
        sme2 = Number(sme1.replace(/\D+/g,""))+20;
        //www1 = sme2 * kol_team;
        kkk1 = Math.floor(wid2 / sme2);         kkk2 = wid2 / sme2;
        eee1 = kkk1 * sme2;
        //$('.car-wrap').css('width', eee1+'px');
        mmm1 = kol_team - kkk1;
        //console.log('resize =>>= wid2='+wid2 +' sme2='+sme2  +' kkk1='+kkk1 +'  mmm1='+mmm1 +'left_team='+left_team);
        aaa = '=>>= окно wid1='+wid1+' , wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 + '='+kkk2 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
        //$('#otl').text(aaa);
        if (left_team > 0) {
            sme3 = left_team * sme2;
            //$('#team_1').css('margin-left', '-'+sme3+'px');
        };
    });
});
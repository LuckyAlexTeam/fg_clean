<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site/bootstrap.min.css',
        'css/site/screen.css',
        'css/site/customers-style.css',
        'css/site/form.css',
        'css/site/main.css',
        'css/site/fonts/awesome/css/font-awesome.css',
    ];
    public $js = [
        // 'js/site/jquery.cookie.js',
        // 'js/site/jquery-ui.js',
        // 'js/site/jquery-2.1.4.min.js',
        'js/site/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

<?php
namespace frontend\controllers;

use Yii;
// use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
// use frontend\models\PasswordResetRequestForm;
// use frontend\models\ResetPasswordForm;
// use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\LoginForm;
use frontend\models\Registration;
use frontend\models\ChangePassword;
use frontend\models\AdminContacts;

//анкеты
use frontend\models\questionary\General;
use frontend\models\questionary\Agency;
use frontend\models\questionary\Designer;
use frontend\models\questionary\Models;
use frontend\models\questionary\Photographer;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (($action->id == 'settings') &&
            ($action->id == 'login')) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {


        $contactForm = new ContactForm;
        $contactFormItems = $contactForm->getCategoryQuestions();
        $contactFormOk = FALSE;

        if ($contactForm->load(Yii::$app->request->post()) && $contactForm->validate()) {
            if($contactForm->sendEmail()){
                $contactFormOk = TRUE;
            } else{
                die('ошибка при отправке письма');
            }
        }

        $user = new Registration;
        return $this->render('index', [
            'contactForm'       => $contactForm,
            'contactFormItems'  => $contactFormItems,
            'contactFormOk'     => $contactFormOk,

            'registration' => $user,
            'accountsType' => $user->getAccountsType(),
            'sel_profs'    => $user->sel_profs()
        ]);
    }

    /*
    * Registration new User
    *
    */
    public function actionRegistration()
    {
        $user = new Registration;
        // var_dump(Yii::$app->request->post()); die();
        if (!($user->load(Yii::$app->request->post()) && $user->registration())) {
            return print_r($user->getErrors());
        }
        return $this->redirect('../settings');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    // public function actionLogout()
    // {
    //     Yii::$app->user->logout();

    //     return $this->goHome();
    // }



    /**
     * Signs user up.
     *
     * @return mixed
     */
    // public function actionSignup()
    // {
    //     $model = new SignupForm();
    //     if ($model->load(Yii::$app->request->post())) {
    //         if ($user = $model->signup()) {
    //             if (Yii::$app->getUser()->login($user)) {
    //                 return $this->goHome();
    //             }
    //         }
    //     }

    //     return $this->render('signup', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    // public function actionRequestPasswordReset()
    // {
    //     $model = new PasswordResetRequestForm();
    //     if ($model->load(Yii::$app->request->post()) && $model->validate()) {
    //         if ($model->sendEmail()) {
    //             Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

    //             return $this->goHome();
    //         } else {
    //             Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
    //         }
    //     }

    //     return $this->render('requestPasswordResetToken', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    // public function actionResetPassword($token)
    // {
    //     try {
    //         $model = new ResetPasswordForm($token);
    //     } catch (InvalidParamException $e) {
    //         throw new BadRequestHttpException($e->getMessage());
    //     }

    //     if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
    //         Yii::$app->session->setFlash('success', 'New password was saved.');

    //         return $this->goHome();
    //     }

    //     return $this->render('resetPassword', [
    //         'model' => $model,
    //     ]);
    // }





    //Добавленные страниц
    public function actionQuestionary()
    {
        $general = new General();

        $agency = FALSE;
        $designer = FALSE;
        $models = FALSE;
        $photographer = FALSE;


        // $agency = new Agency();
        // $designer = new Designer();
        // $models = new Models();
        $photographer = new Photographer();

        return $this->render('questionary', [
            'general' => $general,
            'models' => $models,
            'photographer' => $photographer,

            'agency' => $agency,
            'designer' => $designer
            ]);
    }

    public function actionAlbum()
    {
        return $this->render('album');
    }

    public function actionJournal()
    { //Нужно подключить css

        return $this->render('journal');
    }

    // public function actionVisitor()
    // {
    //     // $this->layout('visitor');
    //     // var_dump( $this->layout);die();
    //     return $this->render('settings');   //переработать!!!
    // }

    public function actionSearch()
    {
        return $this->render('search');
    }

    public function actionSettings()
    {
        $msg  = "";
        if ($post = Yii::$app->request->post()) {
            $form = $post['Users'];
            $password_form = ChangePassword::findIdentity(Yii::$app->user->getId());
            $password_form->old_password = $form['old_password'];
            $password_form->new_password = $form['new_password'];
            $password_form->new_password_repeat = $form['repeat_password'];
            if (!$msg = $password_form->change()) {
                $msg = "Пароль успешно изменен";
            }
        }
        return $this->render('settings', [
            'msg' => $msg]);
    }

    // public function actionChangepassword()
    // {
    //     $form = Yii::$app->request->post()['Users'];

    //     $password_form = ChangePassword::findIdentity(Yii::$app->user->getId());
    //     // var_dump($password_form);die();
    //     $password_form->old_password = $form['old_password'];
    //     $password_form->new_password = $form['new_password'];
    //     $password_form->new_password_repeat = $form['repeat_password'];
    //     if (!$msg = $password_form->change()) {
    //         $msg = "Пароль успешно изменен";
    //     }
    //     return $this->render('settings', ['msg' => $msg]);
    // }

    public function actionEvents()
    {
        return $this->render('events');
    }

    public function actionVacancies()
    {
        return $this->render('vacancies');
    }

    public function actionSubscription()
    {
        return $this->render('subscription');
    }

    public function actionRecommendations()
    {
        return $this->render('recommendations');
    }

    // public function actionMypage() // переделать по ID
    // {
    //     return $this->render('mypage');
    // }

    public function actionMypage()
    {

    }



}

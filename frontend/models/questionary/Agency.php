<?php

namespace frontend\models\questionary;

use Yii;
use common\models\User;

/**
 * This is the model class for table "agency".
 *
 * @property integer $user_id
 * @property string $adress
 * @property string $name
 * @property string $social_media
 * @property string $age
 * @property string $model_сategory
 * @property string $other_requipments
 * @property integer $professional_experience
 * @property integer $total_models
 * @property string $specialization
 * @property string $main_reason_on_site
 *
 * @property User $user
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adress', 'name', 'age', 'model_сategory', 'professional_experience', 'total_models', 'specialization'], 'required'],
            [['professional_experience', 'total_models'], 'integer'],
            [['other_requipments', 'specialization'], 'string'],
            [['adress', 'name', 'social_media', 'age', 'model_сategory', 'main_reason_on_site'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'id пользователя'),
            'adress' => Yii::t('app', 'Адрес'),
            'name' => Yii::t('app', 'Название'),
            'social_media' => Yii::t('app', 'Официальные социальные медиа'),
            'age' => Yii::t('app', 'Возрастные группы'),
            'model_category' => Yii::t('app', 'Категории моделей'),
            'other_requipments' => Yii::t('app', 'Другие требования'),
            'professional_experience' => Yii::t('app', 'Опыт деятельности'),
            'total_models' => Yii::t('app', 'Общее число моделей'),
            'specialization' => Yii::t('app', 'Тип агенства (специализация)'),
            'main_reason_on_site' => Yii::t('app', 'Основные причины быть на сайте'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

<?php

namespace frontend\models\questionary;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\User;

use frontend\models\locations;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $desc
 * @property string $phone
 * @property string $phone2
 * @property string $lang
 * @property string $email
 * @property integer $status
 * @property string $image_main
 * @property string $images
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $city_id
 * @property string $skype
 * @property string $web-site
 * @property integer $gender
 * @property integer $what_show
 * @property integer $date_of_birth
 * @property string $locations
 * @property string $member_since
 * @property string $last_login
 *
 * @property Agency $agency
 * @property Designer $designer
 * @property FashionExpert $fashionExpert
 * @property Models $models
 * @property Photographer $photographer
 */
class General extends User
{
    //Gender
     const MALE = 1;
     const FEMALE = 2;
     const MALE_FEMALE = 3;

     // What_show
     const AGE = 1;
     const BIRTHDAY = 2;


    public $genders =  array(
        // self::MALE => Yii::t('app', 'Мужской'),
        // self::FEMALE => Yii::t('app', 'Женский'));
        1 => 'Мужской',
        2 => 'Женский');

    public $what_show =  array(
        // self::AGE => Yii::t('app', 'Возраст'),
        // self::BIRTHDAY => Yii::t('app', 'Дата рождения')
        1 => 'Возраст',
        2 => 'Дата рождения'
        );


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone', 'email', 'skype', 'gender'], 'required'],
            [['gender'], 'in', 'range' => [self::MALE, self::FEMALE, self::MALE_FEMALE]],
            [['first_name', 'last_name', 'skype'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['agency_info'], 'string'],
            [['web-site'], 'string'],
            [['date_of_birth'], 'date', 'format' => 'YYYY-MM-DD'],
            [['what_show'], 'in', 'range' => [self::AGE, self::BIRTHDAY]],
            ['locations', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->getLocationsIds()]], // each value in x can only be 2, 4, 6 or 8
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('app', 'Имя'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Email'),
            'skype' => Yii::t('app', 'Skype'),
            'web-site' => Yii::t('app', 'Веб-сайт'),
            'gender' => Yii::t('app', 'Пол'),
            'what_show' => Yii::t('app', 'Что показывать (возраст/дата рождения)'),
            'date_of_birth' => Yii::t('app', 'Дата рождения'),
            'locations' => Yii::t('app', 'Локации'),
            'member_since' => Yii::t('app', 'Дата регистрации'),
            'last_login' => Yii::t('app', 'Последний вход'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesigner()
    {
        return $this->hasOne(Designer::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFashionExpert()
    {
        return $this->hasOne(FashionExpert::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasOne(Models::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotographer()
    {
        return $this->hasOne(Photographer::className(), ['user_id' => 'id']);
    }

    public function getLocationsIds()
    {
        $locs_raw = Locations::find()->select('id')->asArray()->all();
        foreach ($locs_raw as $value) {
            $locs [] = $value['id'];
        }
        return $locs;
    }

    public function getLocations()
    {
        $locs_raw = Locations::find()->select(['city', 'id'])->asArray()->all();
        $locs = ArrayHelper::map($locs_raw, 'id', 'city');
        return $locs;
    }


}

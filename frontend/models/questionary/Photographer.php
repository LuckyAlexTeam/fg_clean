<?php

namespace frontend\models\questionary;

use Yii;
use common\models\User;

/**
 * This is the model class for table "photographer".
 *
 * @property integer $user_id
 * @property integer $professional_experience
 * @property string $work_areas
 * @property string $work_type
 * @property string $camera_equipment
 * @property integer $sudio
 * @property integer $makeup_artist
 * @property integer $hair_stylist
 * @property string $compensation
 * @property string $rates_for_photoshoot_hour
 * @property string $rates_for_photoshoot_half
 * @property string $rates_for_photoshoot_full
 * @property integer $travel_ability
 * @property string $main_reason_on_site
 *
 * @property User $user
 */
class Photographer extends \yii\db\ActiveRecord
{
    public $_professional_experience = [
        1 => '1-5 years',
        2 => '5-10 years',
        3 => 'More than 10 years',
    ];
    public $_work_areas = [
        1 => 'Advertising photographers',
        2 => 'Beauty & Hair photographers',
        3 => 'Black & White photographers',
        4 => 'Children photographers',
        5 => 'Celebrity/Entertainment',
        6 => 'Fashion/Editoral photographers',
        7 => 'Nude',
        8 => 'People & lifestyle photographers',
        9 => 'Portrait photographers',
        10 => 'Sports/Action photographers',
        11 => 'Reportage photographers',
        12 => 'Runway/Fashion Event photographers',
        13 => 'Swimwear & lingerie photographers',
        14 => 'Travel photographers',
        15 => 'Underwater photographers',
        16 => 'Photo post production (retouching)',
        17 => 'Film/video production',
        18 => 'Commercial production:',
        19 => 'TV/video production',
        20 => 'Film/video Post Production & CGI',
        21 => 'CGI & 3D:',
        22 => 'Motion graphics',
        23 => 'Sound design/scoring',
        24 => 'Rental studios'
    ];

    public $_work_type = [
        1 => 'Editorials',
        2 => 'Runway',
        3 => 'Beauty',
        4 => 'Portrairs / Celebrity',
        5 => 'Glamour / Nude',
        6 => 'Commercial / Advertising',
        7 => 'Street Fashion',
        8 => 'Creative projects',
        9 => 'Test Shoot',
        10 => 'Other',
    ];

    public $_studio = [
        1 => 'Provided',
        2 => 'Not provided',
    ];

    public $_makeup_artist = [
        1 => 'Provided',
        2 => 'Not provided',
    ];

    public $_hair_stylist = [
        1 => 'Provided',
        2 => 'Not provided',
    ];

    public $_compensation = [
        1 => 'Payment Only',
        2 => 'Photo/Time For Print',
        3 => 'Test shoot',
        4 => 'Negotiable'
    ];

    public $_travel_ability = [
        1 => 'To search for work offers',
        2 => 'Work withn location',
    ];

    public $_main_reason_on_site = [
        1 => 'To search for work offers',
        2 => 'To sign up for master classes',
        3 => 'To carry out master classes',
        4 => 'To search for photographers',
        5 => 'To participate in competitions and castings',
        6 => 'Fashion Weeks, ShowRooms',
        7 => 'To monitor the fashion news',
        8 => 'To establish new contracts',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photographer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'professional_experience', 'work_areas', 'work_type', 'studio', 'makeup_artist', 'hair_stylist', 'compensation', 'travel_ability'], 'required'],
            ['professional_experience', 'in', 'range' => range(1, max(array_keys($this->_professional_experience)))],
            ['work_areas','each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_work_areas]],
            ['work_type','each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_work_type]],
            ['main_reason_on_site','each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_main_reason_on_site]],
            [['camera_equipment'], 'string'],
            ['studio', 'in', 'range' => range(1, max(array_keys($this->_studio)))],
            ['makeup_artist', 'in', 'range' => range(1, max(array_keys($this->_makeup_artist)))],
            ['hair_stylist', 'in', 'range' => range(1, max(array_keys($this->_hair_stylist)))],
            ['compensation', 'in', 'range' => range(1, max(array_keys($this->_compensation)))],
            ['travel_ability', 'in', 'range' => range(1, max(array_keys($this->_travel_ability)))],
            ['main_reason_on_site','each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_main_reason_on_site]],

            [[ 'professional_experience', 'studio', 'makeup_artist', 'hair_stylist', 'travel_ability', 'rates_for_photoshoot_hour', 'rates_for_photoshoot_half', 'rates_for_photoshoot_full',], 'integer'],
            [['work_areas', 'work_type', 'compensation',  'main_reason_on_site'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'id пользователя'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'work_areas' => Yii::t('app', 'Отрасли работы'),
            'work_type' => Yii::t('app', 'Виды работ'),
            'camera_equipment' => Yii::t('app', 'Оборудование (камера)'),
            'sudio' => Yii::t('app', 'Студия предоставлена'),
            'makeup_artist' => Yii::t('app', 'Мэйкап предоставлен'),
            'hair_stylist' => Yii::t('app', 'Парикмахер (стилист) предоставлен'),
            'сompensation' => Yii::t('app', 'Компенсации'),
            'rates_for_photoshoot_hour' => Yii::t('app', 'Цена за фотосессию (в час)'),
            'rates_for_photoshoot_half' => Yii::t('app', 'Цена за фотосессию (за пол дня / 4 часа)'),
            'rates_for_photoshoot_full' => Yii::t('app', 'Цена за фотосессию (полный день / 8 часов)'),
            'travel_ability' => Yii::t('app', 'Готовность к переезду'),
            'main_reason_on_site' => Yii::t('app', 'Основные причины быть на сайте'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

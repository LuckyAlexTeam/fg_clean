<?php

namespace frontend\models\questionary;

use Yii;
use common\models\User;

/**
 * This is the model class for table "designer".
 *
 * @property integer $user_id
 * @property string $work_type
 * @property integer $contracted_or_not
 * @property string $work_place
 * @property string $education
 * @property integer $professional_experience
 * @property string $сompensation
 * @property string $languages
 * @property integer $travel_ability
 * @property string $main_reason_on_site
 *
 * @property User $user
 */
class Designer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'designer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'work_type', 'contracted_or_not', 'professional_experience', 'languages', 'travel_ability'], 'required'],
            [['user_id', 'contracted_or_not', 'professional_experience', 'travel_ability'], 'integer'],
            [['work_place', 'education'], 'string'],
            [['work_type', 'сompensation', 'languages', 'main_reason_on_site'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'id пользователя'),
            'work_type' => Yii::t('app', 'Отрасль работы'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'work_place' => Yii::t('app', 'Рабочее место'),
            'education' => Yii::t('app', 'Образование, профессиональные курсы, мастер классы'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'сompensation' => Yii::t('app', 'Компенсации'),
            'languages' => Yii::t('app', 'Владение языками'),
            'travel_ability' => Yii::t('app', 'Готовность к переезду'),
            'main_reason_on_site' => Yii::t('app', 'Основные причины быть на сайте'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

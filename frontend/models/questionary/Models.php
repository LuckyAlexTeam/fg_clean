<?php

namespace frontend\models\questionary;

use Yii;
use common\models\User;

/**
 * This is the model class for table "models".
 *
 * @property integer $user_id
 * @property integer $legal_agree
 * @property string $agency_info
 * @property integer $height
 * @property integer $weight
 * @property integer $chest
 * @property integer $waist
 * @property integer $hips
 * @property integer $dress
 * @property integer $сup
 * @property integer $shoe
 * @property string $ethnicity
 * @property integer $skin_сolor
 * @property integer $eye_сolor
 * @property integer $natural_hair_color
 * @property integer $hair_сoloration
 * @property integer $readness_to_change_color
 * @property integer $hair_lenght
 * @property string $tatoos
 * @property string $piercing
 * @property string $model_сategory
 * @property integer $professional_experience
 * @property string $additional_skils
 * @property string $languages
 * @property string $сompensation
 * @property string $rates_for_photoshoot_hour
 * @property string $rates_for_photoshoot_half
 * @property string $rates_for_photoshoot_full
 * @property string $rates_for_runway
 * @property integer $contracted_or_not
 * @property integer $travel_ability
 * @property string $main_reason_on_site
 *
 * @property User $user
 */
class Models extends \yii\db\ActiveRecord
{

    public $metrical_sys = [0 => 'cm',
                            1 => 'in'];
    public $metrical;

    public $_ethnicity = [  'African',
                            'Asian',
                            'European',
                            'Hispanic',
                            'Indian',
                            'Latin America',
                            'Mediterranean',
                            'Near East',
                            'Scandinavian',
                            'Mixed',
                            'Other:'
                            ];
    public $ethnicity_input;

    public $cups = [1 => 'A',
                    2 => 'B',
                    3 => 'C',
                    4 => 'D',
                    5 => 'DD',
                    6 => 'DDD',
                    7 => 'DDDD'
                    ];

    public $skin = [1 => 'Pale white',
                    2 => 'White',
                    3 => 'Olive',
                    4 => 'Light brown',
                    5 => 'Brown',
                    6 => 'Dark brown',
                    7 => 'Black',
                    ];

    public $eye =  [1 => 'Black',
                    2 => 'Blue',
                    3 => 'Green',
                    4 => 'Hazel',
                    5 => 'Other',
                    ];

    public $hair = [1 => 'Blonde',
                    2 => 'Strawberry Blonde',
                    3 => 'Auburn',
                    4 => 'Ginder',
                    5 => 'Light Brown',
                    6 => 'Brown',
                    7 => 'Dark Brown',
                    8 => 'Salt & Pepper',
                    9 => 'Black',
                    10 => 'Other',
                    ];
    public $_hair_coloration = [ 1 => 'Not colored',
                                2 => 'Blonde',
                                3 => 'Strawberry Blonde',
                                4 => 'Auburn',
                                5 => 'Ginder',
                                6 => 'Red',
                                7 => 'Light Brown',
                                8 => 'Brown',
                                9 => 'Dark Brown',
                                10 => 'Salt & Pepper',
                                11 => 'Black',
                                12 => 'Black blue',
                                13 => 'Other',
                                ];
    public $_readness_to_change_color = [   1 => 'Ready to change hair color',
                                            2 => 'Not eady to change hair color'];
    public $_hair_lenght = [ 1 => 'Bald',
                            2 => 'Short',
                            3 => 'Medium',
                            4 => 'Shoulder lenght',
                            5 => 'Very long',
                            ];
    public $_tatoos = [ // NULL => 'No tatoos',
                        2 => 'Neck',
                        3 => 'Shoulder',
                        4 => 'Biceps',
                        5 => 'Wrist',
                        6 => 'Chest',
                        7 => 'Belly',
                        8 => 'Back',
                        9 => 'Loin',
                        10 => 'Hip',
                        11 => 'Shin',
                        12 => 'Ankle',
                        13 => 'Foot',
                        14 => 'Other'
                        ];
    public $has_tatoos = [  1 => 'No tatoos',
                            0 => 'Has tatoos'
        ];
    public $_piercing = [//NULL => 'No piercing',
                        2 => 'ear tunnels',
                        3 => 'eyebrows',
                        4 => 'tongue',
                        5 => 'nose',
                        6 => 'nipple',
                        7 => 'navel',
                        8 => 'body implants',
                        9 => 'others'
                        ];
    public $has_piercing = [ 1 => 'No piercing',
                             0 => 'Has piercing'
        ];

    public $_category = [
               1  => 'Alternative model',
               2  => 'Body Paint',
               3  => 'Body Parts:',
               4     => 'Ears',
               5     => 'Eyes',
               6     => 'Fingers',
               7     => 'Foot',
               8     => 'Hair',
               9     => 'Hands',
               10    => 'Lips',
               11    => 'Neck',
               12 => 'Print photo model:',
               13    => 'Editorial',
               14    => 'Print ads',
               15 => 'Hight fashion model',
               16 => 'Runway model',
               17 => 'Glamour model:',
               18    => 'Lingerie/Body model',
               19    => 'Swimwear model',
               20 => 'Fitness model',
               21 => 'Beauty model (hair/makeup)',
               22 => 'Nude',
               23 => 'Promotional model',
               24 => 'Plus-sized model',
               25 => 'Street Fashion',
               26 => 'Sport model',
               27 => 'Senior model (40+)',
               28 => 'Video'
    ];


    public $_professional_experience = [
            1 => 'Up to 1 year',
            2 => '1-5 years',
            3 => 'More than 5 years',
        ];
    public $_additional_skils = [
            1 => 'Actor or Actress',
            2 => 'Film Acting',
            3 => 'TV Acting',
            4 => 'Dance',
            5 => 'Music',
            6 => 'Extreme Sports',
            7 => 'Stage Acting',
            8 => 'Commercical Acting',
            9 => 'Singer',
            10 => 'Prfomance Artists',
            11 => 'Voice Over Talent',
        ];
    public $_languages = [
            1 => 'Chinese',
            2 => 'English',
            3 => 'French',
            4 => 'German',
            5 => 'Italian',
            6 => 'Russian',
            7 => 'Spanish',
            8 => 'Ukrainian',
            9 => 'Others',
        ];
    public $_compensation = [
            1 => 'Payment Only',
            2 => 'Photo/Time For Print',
            3 => 'Test shoot',
            4 => 'Negotiable'
            ];
    public $_contracted_or_not = [
            1 => 'Contracted with agency',
            2 => 'Not contracted',
        ];
    public $_travel_ability = [
            1 => 'To search for work offers',
            2 => 'Work withn location',
        ];
    public $_main_reason_on_site = [
            1 => 'To search for work offers',
            2 => 'To sign up for master classes',
            3 => 'To carry out master classes',
            4 => 'To search for photographers',
            5 => 'To participate in competitions and castings',
            6 => 'Fashion Weeks, ShowRooms',
            7 => 'To monitor the fashion news',
            8 => 'To establish new contracts',
        ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'models';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['height', 'weight', 'waist', 'hips', 'chest', 'dress', 'shoe', 'ethnicity', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'model_category', 'professional_experience', 'languages', 'compensation', 'contracted_or_not', 'travel_ability'], 'required'],
            ['height', 'number', 'min' => 61, 'max' => 227],
            ['weight', 'number', 'min' => 30, 'max' => 170],
            ['chest', 'number', 'min' => 53, 'max' => 230],
            ['waist', 'number', 'min' => 48, 'max' => 134],
            ['hips', 'number', 'min' => 48, 'max' => 134],
            ['dress', 'number', 'min' => 32, 'max' => 60],
            ['cup', 'in', 'range' => range(1, max(array_keys($this->cups)))],
            ['shoe', 'number', 'min' => 34, 'max' => 47],
            ['ethnicity', 'string'],
            ['skin_color', 'in', 'range' => array_keys($this->skin)],
            ['eye_color', 'in', 'range' => array_keys($this->eye)],
            ['natural_hair_color', 'in', 'range' => array_keys($this->hair)],
            ['hair_coloration', 'in', 'range' => array_keys($this->_hair_coloration)],
            ['readness_to_change_color', 'in', 'range' => array_keys($this->_readness_to_change_color)],
            ['hair_lenght', 'in', 'range' => array_keys($this->_hair_lenght)],
            ['tatoos', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_tatoos]],
            ['piercing', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_piercing]],
            ['model_category', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_piercing]],
            ['professional_experience', 'in', 'range' => range(1, max(array_keys($this->_professional_experience)))],
            ['additional_skils', 'each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_additional_skils]],
            ['languages','each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_languages]],
            ['compensation', 'in', 'range' => range(1, max(array_keys($this->_compensation)))],
            ['contracted_or_not', 'in', 'range' => range(1, max(array_keys($this->_contracted_or_not)))],
            ['travel_ability', 'in', 'range' => range(1, max(array_keys($this->_travel_ability)))],
            ['main_reason_on_site','each', 'rule' => ['in', 'allowArray' => true, 'range' => $this->_main_reason_on_site]],

            [['legal_agree', 'height', 'weight', 'chest', 'waist', 'hips', 'dress', 'cup', 'shoe', 'skin_color', 'eye_color', 'natural_hair_color', 'hair_coloration', 'readness_to_change_color', 'hair_lenght', 'professional_experience', 'contracted_or_not', 'travel_ability', 'rates_for_photoshoot_hour', 'rates_for_photoshoot_half', 'rates_for_photoshoot_full'], 'integer'],
            [['agency_info', 'rates_for_runway'], 'string'],
            [['ethnicity', 'tatoos', 'piercing', 'model_category', 'additional_skils', 'languages', 'compensation',  'main_reason_on_site'], 'string', 'max' => 255]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'id пользователя'),
            'legal_agree' => Yii::t('app', 'Разрешение работать (до 18 лет)'),
            'agency_info' => Yii::t('app', 'Информация об агенстве'),
            'height' => Yii::t('app', 'Рост'),
            'weight' => Yii::t('app', 'Вес'),
            'chest' => Yii::t('app', 'Объем груди'),
            'waist' => Yii::t('app', 'Объем талии'),
            'hips' => Yii::t('app', 'Объем бедер'),
            'dress' => Yii::t('app', 'Размер одежды, EU'),
            'cup' => Yii::t('app', 'Размер чашечки/груди'),
            'shoe' => Yii::t('app', 'Размер обуви, EU'),
            'ethnicity' => Yii::t('app', 'Национальность'),
            'skin_color' => Yii::t('app', 'Цвет кожи'),
            'eye_color' => Yii::t('app', 'Цвет глаз'),
            'natural_hair_color' => Yii::t('app', 'Натуральный цвет волос'),
            'hair_coloration' => Yii::t('app', 'Покрашены ли волосы'),
            'readness_to_change_color' => Yii::t('app', 'Готовность изменить цвет волос'),
            'hair_lenght' => Yii::t('app', 'Длина волос'),
            'tatoos' => Yii::t('app', 'Наличие татуировок'),
            'piercing' => Yii::t('app', 'Наличие пирсинга'),
            'model_category' => Yii::t('app', 'Работа в жанрах'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'additional_skils' => Yii::t('app', 'Дополнительные навыки'),
            'languages' => Yii::t('app', 'Владение языками'),
            'compensation' => Yii::t('app', 'Компенсации'),
            'rates_for_photoshoot_hour' => Yii::t('app', 'Цена за фотосессию (в час)'),
            'rates_for_photoshoot_half' => Yii::t('app', 'Цена за фотосессию (за пол дня / 4 часа)'),
            'rates_for_photoshoot_full' => Yii::t('app', 'Цена за фотосессию (полный день / 8 часов)'),
            'rates_for_runway' => Yii::t('app', 'Цены за переезд'),
            'contracted_or_not' => Yii::t('app', 'Контракт через агенство'),
            'travel_ability' => Yii::t('app', 'Готовность к переезду'),
            'main_reason_on_site' => Yii::t('app', 'Основные причины быть на сайте'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

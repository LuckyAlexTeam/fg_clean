<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "admin_contacts".
 *
 * @property string $category_question
 * @property string $email
 */
class AdminContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_question', 'email'], 'required'],
            [['category_question', 'email'], 'string', 'max' => 255],
            [['category_question'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    // public function attributeLabels()
    // {
    //     return [
    //         'category_question' => 'Category Question',
    //         'email' => 'Email',
    //     ];
    // }

}

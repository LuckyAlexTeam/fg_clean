<?php
use yii\helpers\Url;
?>
<!-- Навигация -->
<div class="vis-nav">
    <div <?php if (Yii::$app->controller->action->id == 'settings') echo 'class="ch"'?>>
        <a href="<?= Url::to(['settings'])?>">Settings</a>
    </div>
    <div <?php if (Yii::$app->controller->action->id == 'search') echo 'class="ch"'?>>
        <a href="<?= Url::to(['search'])?>">Search</a>
    </div>
    <div <?php if (Yii::$app->controller->action->id == 'journal') echo 'class="ch"'?>>
        <a href="<?= Url::to(['journal'])?>">Journal</a>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".krutilka_page1_key").click(function(){
			$(".krutilka_page1").attr("style","display:block;");
			$(".krutilka_page2").attr("style","display:none;");
			$(".nav-s-item").removeClass("active");
			$(".krutilka_page1_key").addClass("active");
		});
		$(".krutilka_page2_key").click(function(){
			$(".krutilka_page2").attr("style","display:block;");
			$(".krutilka_page1").attr("style","display:none;");
			$(".nav-s-item").removeClass("active");
			$(".krutilka_page2_key").addClass("active");
		});
	});
</script>

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
echo $this->render('_visitor_nav') ?>

<div class="person-wrapper visitor">
	<div class="set-wrapper">
		<h2>Settigs</h2>
		<span class="hr"></span>
		<nav class="nav-settings">
			<div class="nav-s-item krutilka_page1_key active" href="pass">Password</div>
			<div class="nav-s-item krutilka_page2_key" href="type">Account Type</div>
		</nav>
		<div class="pass hide">


	<!-- <div class="pass hide"> -->
		<h2>Сменить пароль:</h2>
		<br>
		<!-- <input class="pass-item" type="password" placeholder="Старый пароль"> -->
		<!-- <input class="pass-item" type="password" placeholder="Новый пароль"> -->
		<!-- <input class="pass-item" type="password" placeholder="Повторите пароль"> -->

		<!-- <input class="pass-btn" name="save" value="save" type="submit"> -->
		<!-- <input class="pass-btn" name="cancel" value="cancel" type="submit"> -->

	</div>
</div>
		<div class="type">

<div id="profile_error_password_container"></div>
<div class="krutilka_page1">
	<?php /*$form = ActiveForm::begin([
                    'action' => Url::to(['changepassword']),
                        'id' => 'form_reg',
                        'method' => 'post',
                        'class' => "form_password",
                           // 'error' => false,
                        'fieldConfig' => [
                           'inputTemplate' => '<div class="textright">:<div class="textred">*</div></div>
												<div>
													{input}
												</div>',

                        ]
                    // 'enableAjaxValidation'   => true,
                    // 'enableClientValidation' => false,
                    // 'validateOnBlur'         => false,
                    // 'validateOnType'         => false,
                    // 'validateOnChange'       => false,
                    // 'validateOnSubmit'       => true,
                ]) ?>
        <?= $form->field($password_form, 'old_password')->textInput( [
            'class' => 'pass-item '
        ]) ?>
        <?= $form->field($password_form, 'new_password')->textInput( [
            'class' => 'pass-item '
        ]) ?>
        <?= $form->field($password_form, 'repeat_password')->textInput( [
            'class' => 'pass-item '
        ]) ?>
        <?= Html::submitButton('save', ['class' => 'pass-btn']) ?>
        <?= Html::resetButton('cancel', ['class' => 'pass-btn']) ?>
	<?php $form = ActiveForm::End() */?>
 	     <form class="form_password" id="form-password" action="<?= URL::to(['site/settings']);?>" method="post">
			<div>
				<div class="textright">
					Old Password:<div class="textred">*</div>
				</div>
				<div>
					<input type="password" name="Users[old_password]" id="passwordold" placeholder="">
				</div>
			</div>
			<div>
				<div class="textright">
					New Password:<div class="textred">*</div>
				</div>
				<div>
					<input type="password" name="Users[new_password]" id="passwordnew" placeholder="">
				</div>
			</div>
			<div>
				<div class="textright">
					Repeat Password:<div class="textred">*</div>
				</div>
				<div>
					<input type="password" name="Users[repeat_password]" id="passwordtwo" placeholder="">
				</div>
			</div>
            <div><?= $msg ?></div>
			<div class="exeptleft">
				<hr>
				Fields macked with <div class="textred">*</div> are mandatory
			</div>
			<div class="exeptpadding">
				<input type="submit" id="save" value="save">
				<input type="reset" value="cancel"></input>
				<!-- <a href="#"></a> -->
			</div>
		</form>
</div>

<div class="type krutilka_page2" style="display:none;">

<div class="price-container">
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
						АККАУНТ<!-- Account -->
						<br>
						<span class="price-name">КЛАССИЧЕСКИЙ<!-- Classic --></span>
						<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>$25</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">3 ИЗДАНИЯ ВАКАНСИЙ<br>
20 СОВМЕСТНАЯ ПРЕДЛОЖЕНИЯ<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ОГРАНИЧЕНОЙ в профайле<br>
LIMITED ИЗГОТОВЛЕНИЕ<br></p>
						<a href="#popup-register" data-tarif="5" class="price-btn" id="join1">РЕГИСТРАЦИЯ<!-- JOIN --></a>
						<div class="clear"></div>
				</div>
			</div>

			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ<!-- Account -->
					<br>
					<span class="price-name">ВИП<!-- VIP --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>$250</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>
До 4-х очерков В ГОД<br>
3 участников событий различного формата В ГОД<br>
20 ИЗДАНИЯ ВАКАНСИЙ<br>
Неограниченные предложения о сотрудничестве<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>
ПОЛНЫЙ возможности настройки<br></p>
					<a href="#popup-register" data-tarif="6" class="price-btn" id="join2">РЕГИСТРАЦИЯ<!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>

			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ<!-- Account -->
					<br>
					<span class="price-name">КОРПОРАТИВНЫЙ<!-- Corporate --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>$350</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>
До 4-х очерков В ГОД<br>
3 участников событий различного формата В ГОД<br>
20 ИЗДАНИЯ ВАКАНСИЙ<br>
Неограниченные предложения о сотрудничестве<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>
ПОЛНЫЙ возможности настройки<br>
20 ДОПОЛНИТЕЛЬНЫЕ CLASSIC ПРОФИЛИ для сотрудников Вашей компании<br></p>
					<a href="#popup-register" data-tarif="7" class="price-btn" id="join3">РЕГИСТРАЦИЯ<!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>


</div>
	</div>
</div>


<script>
   //  $('#save').on('click',function(){
   //     $.ajax({
   //          type: 'POST',
   //          url:"<?= Url::to(['site/changepassword']);?>",
   //          data: $("#form-password").serialize(),
   //          success: function(data) {
   //                  $('#profile_error_password_container').html(data);
   //          }
   //      }).fail(function(){
   //          alert('Ошибка обработки данных');
   //      })
   //      return false;
   // });
</script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\agency */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin([
	// 'fieldConfig' => ['options' => ['style' => 'text-color:black']]
	]); ?>

	<h2>Основная часть</h2>
	<?= $this->render('../questionary/_general', [
		'general' => $general,
		'form'	 => $form,
		'models' => $models
		]) ?>

	<?php if ($models): ?>
		<h2>Анкета модели</h2>
		<?= $this->render('../questionary/_models', [
			'form'	 => $form,
			'models' => $models
			]) ?>
	<?php endif ?>

	<?php if ($photographer): ?>
		<h2>Анкета фотографа</h2>
		<?= $this->render('../questionary/_photographer', [
			'form'	 => $form,
			'photographer' => $photographer
		]) ?>
	<?php endif ?>

<?php $form = ActiveForm::end(); ?>

<!--
	<h2>Анкета агенства</h2>
	< ?= $this->render('../questionary/_agency', [
		'agency' => $agency,
		'form'	 => $form
		]) ?>

	<h2>Анкета дизайнера</h2>
	< ?= $this->render('../questionary/_designer', [
		'designer' => $designer,
		'form'	 => $form
		]) ?> -->




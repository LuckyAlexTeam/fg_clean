<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\FashionExpert */
/* @var $form ActiveForm */
?>
<div class="questionary-_fashion_expert">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_id') ?>
        <?= $form->field($model, 'specialization') ?>
        <?= $form->field($model, 'languages') ?>
        <?= $form->field($model, 'travel_ability') ?>
        <?= $form->field($model, 'main_reason_on_site') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- questionary-_fashion_expert -->

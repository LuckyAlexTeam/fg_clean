<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $photographer frontend\models\questionary\Photographer */
/* @var $form ActiveForm */
?>
<div class="questionary-_photographer">

        <?= $form->field($photographer, 'professional_experience')->dropDownList($photographer->_professional_experience) ?>

        <?= $form->field($photographer, 'work_areas')->checkBoxList($photographer->_work_areas) ?>
        <?= $form->field($photographer, 'work_type')->checkBoxList($photographer->_work_type) ?>
        <?= $form->field($photographer, 'studio')->dropDownList($photographer->_studio) ?>
        <?= $form->field($photographer, 'makeup_artist')->dropDownList($photographer->_makeup_artist) ?>
        <?= $form->field($photographer, 'hair_stylist')->dropDownList($photographer->_hair_stylist) ?>
        <?= $form->field($photographer, 'compensation')->checkBoxList($photographer->_compensation) ?>
        <?= $form->field($photographer, 'travel_ability')->dropDownList($photographer->_travel_ability) ?>
        <?= $form->field($photographer, 'camera_equipment') ?>
        <?= $form->field($photographer, 'rates_for_photoshoot_hour') ?>
        <?= $form->field($photographer, 'rates_for_photoshoot_half') ?>
        <?= $form->field($photographer, 'rates_for_photoshoot_full') ?>
        <?= $form->field($photographer, 'main_reason_on_site')->checkBoxList($photographer->_compensation) ?>


</div><!-- questionary-_photographer -->

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\General */
/* @var $form ActiveForm */
?>
<div class="questionary-_general">

        <?= $form->field($general, 'first_name')->textInput() ?>
        <?= $form->field($general, 'last_name')->textInput() ?>
        <?= $form->field($general, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
              'mask' => '+99 (999) 999 99 99',
            ]); ?>
        <?= $form->field($general, 'email')->textInput() ?>
        <?= $form->field($general, 'skype')->textInput() ?>
        <?= $form->field($general, 'gender')->dropDownList($general->genders) ?>
        <?php if ($models) echo $form->field($models, 'agency_info')->textArea() ?>
        <?= $form->field($general, 'web_site')->textInput() ?>
         <?= $form->field($general, 'date_of_birth')->widget(DatePicker::classname(), [
            'dateFormat' => 'yyyy-MM-dd',
         ]) ?>
        <?= $form->field($general, 'what_show')->radioList($general->what_show) ?>
        <?php if ($models) echo  $form->field($models, 'legal_agree')->textInput([
                                        'id' => 'legal_agree',
                                        'class' => 'hidden'
                                    ]) ?>
        <?= $form->field($general, 'locations')->checkboxList($general->getLocations()) ?>

</div><!-- questionary-_general -->

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\experts\ExpertPhoto */
/* @var $form ActiveForm */
?>
<div class="questionary-_experts-_expert_photo">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'questionary_id') ?>
        <?= $form->field($model, 'work_areas') ?>
        <?= $form->field($model, 'work_type') ?>
        <?= $form->field($model, 'sudio') ?>
        <?= $form->field($model, 'contracted_or_not') ?>
        <?= $form->field($model, 'professional_experience') ?>
        <?= $form->field($model, 'makeup_artist') ?>
        <?= $form->field($model, 'hair_stylist') ?>
        <?= $form->field($model, 'camera_equipment') ?>
        <?= $form->field($model, 'сompensation') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- questionary-_experts-_expert_photo -->

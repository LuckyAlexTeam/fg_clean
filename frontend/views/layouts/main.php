
<?php

use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);

$this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php $this->head() ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="language" content="<?= Yii::$app->language ?>">
	<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<title>МодаVisitor</title>
	<meta name="keywords" content="key index">
	<meta name="description" content="des index">
	<!-- <link rel="stylesheet" href="/fonts/awesome/css/font-awesome.min.css"> -->
	<!--[if IE 7]>
		<link rel="stylesheet" href="/fonts/awesome/css/font-awesome-ie7.min.css">
	<![endif]-->

</head>
<body>
<?php $this->beginBody() ?>
<div class="container">
	<div class="head" style="<?php echo (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ? 'position:fixed;':'';?>">
		<div class="lang">
            <a style="color:#fff;" href="<?= Url::to(['site/language', 'id'=>'en']);?>"><div class='lang_N tlanguage' data='N'>en</div></a>
            <a style="color:#fff;" href="<?= Url::to(['site/language', 'id'=>'ru']);?>"><div class='lang_Y tlanguage' data='Y'>ru</div></a>
            <a style="color:#fff;" href="<?= Url::to(['site/language', 'id'=>'fr']);?>"><div class="lang_N tlanguage" data="N">fr</div></a>
            <a style="color:#fff;" href="<?= Url::to(['site/language', 'id'=>'ch']);?>"><div class="lang_N tlanguage" data="N">ch</div></a>
		</div>
		<div class="title">
			<a class="logo" href="/">
				<span class="slogan"> эксклюзивная презентация в сети</span>
			</a>
			<div class="clear"></div>
		</div>

        <?php if (Yii::$app->user->isGuest) {?>
		<div class="menu1" id="top-menu">
			<ul>
				<li class="active">
					<a href="#b1">ДОМОЙ</a>
				</li>
				<li>
					<a href="#b2">О НАС</a>
				</li>
				<li>
					<a href="#b3">ПРЕИМУЩЕСТВА</a>
				</li>
				<li>
					<a href="#b4">КОМАНДА</a>
				</li>
				<li>
					<a href="#b5">ЦЕНОВЫЕ ПАКЕТЫ</a>
				</li>
				<li>
					<a href="#b6">СВЯЖИТЕСЬ С НАМИ</a>
				</li>
			</ul>
				<div class="clear"></div>
		</div>

        <?php } else {?>
        <div class="c-navigation">
			<div class="c-nav-text" style="margin-right: 20px;">
				<p><a href="<?= Url::to(['site/profile', 'id'=> Yii::$app->user->id]);?>">
					<!--
						задание от Александра 20160811:
						В шапке справа, вместо логина/мейла надпись "Мой профиль".
					-->
					Мой профиль
					<?= Yii::$app->user->identity->first_name ?>
				</a></p>
				<p><a href="<?= Url::to(['site/settings']) ?>">Настройки</a>&nbsp;
                                    <a href="<?= Url::to(['site/logout']);?>">Выход</a></p>
			</div>
			<!-- <div class="c-nav-img">
                             <?php if (!empty(Yii::$app->user->avatar)){?>
				<img style="width:40px;" id="avatar_img_small" alt="Фотография профиля" src="../img_avatar_small/no_avatar.png">
                             <?php } ?>
			</div> -->
		</div>
                <?php }?>
	</div>
	<div class="content">
             <?php echo $content;?>
        </div>



	</div>

        <?php if (!(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index')){?>
        <div class="footer">
                <a href="/write">
                        <i aria-hidden="true" class="fa fa-4x fa-pencil-square-o"></i>
                        <br>write to us
                </a>
                <a href="/about">
                        <i aria-hidden="true" class="fa fa-4x fa-info-circle"></i>
                        <br>about us
                </a>
                <a href="/services">
                        <i aria-hidden="true" class="fa fa-4x fa-cogs"></i>
                        <br>our services
                </a>
                <a href="/mentors">
                        <i aria-hidden="true" class="fa fa-4x fa-users"></i>
                        <br>our mentors
                </a>
                <a href="/faq">
                        <i aria-hidden="true" class="fa fa-4x fa-question-circle"></i>
                        <br>faq
                </a>
                <a href="/contacts">
                        <i aria-hidden="true" class="fa fa-4x fa-envelope-o"></i>
                        <br>contacts
                </a>
        </div>
    <?php } ?>

<script>
// Cache selectors
var lastId,
	topMenu = $("#top-menu"),
	topMenuHeight = topMenu.outerHeight()+15,
	// All list items
	menuItems = topMenu.find("a"),
	// Anchors corresponding to menu items
	scrollItems = menuItems.map(function(){
		var item = $($(this).attr("href"));
		if (item.length) { return item; }
	});
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
